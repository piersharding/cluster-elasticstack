# cluster-elasticstack

Example template for dynamically creating an ElasticStack cluster on Docdker with Ansible.


## Summary

This repo contains an example of building a generic ElasticStack (Elasticsearch and Kibana currently) cluster in conjunction with https://gitlab.com/piersharding/generic-cluster.

Checkout generic-cluster and this repo with:
```
git clone git@gitlab.com:piersharding/generic-cluster.git
cd generic-cluster
git clone git@gitlab.com:piersharding/cluster-elasticstack.git
```

Create a `private_vars.yml` in `/path/to/generic-cluster` like this:
```
---
debug: true
cluster_name: elastic_system_core
data_filesystem: /var/lib/stack-data

genericnode_master:
  name: "master"
  flavor: "m1.large"
  image: "{{ image }}"
  num_nodes: 3

genericnode_worker:
  name: "worker"
  flavor: "m1.small"
  image: "{{ image }}"
  num_nodes: 0
```

Follow the instructions here https://gitlab.com/piersharding/generic-cluster, and run `make build`.
This will create a basic 3 node cluster on OpenStack.

Then deploy the ElastiStack by:
```
cd cluster-elasticstack
make build
```
