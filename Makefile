DEBUG ?= false

all: build

build_stack:
	ansible-playbook -i ../playbooks/inventory_cluster playbooks/stack.yml \
	--extra-vars="debug=$(DEBUG)"

build: build_stack

check: ## Health check
	ip=`grep 'master-0' ../playbooks/inventory_cluster | cut -d '=' -f 2`; \
	curl "http://$${ip}:9200/_cluster/health/?wait_for_status=yellow&timeout=50s&pretty"

nodecheck: ## Node state
	ip=`grep 'master-0' ../playbooks/inventory_cluster | cut -d '=' -f 2`; \
	curl "http://$${ip}:9200/_nodes/process?pretty"

statecheck: ## Whole cluster state
	ip=`grep 'master-0' ../playbooks/inventory_cluster | cut -d '=' -f 2`; \
	curl "http://$${ip}:9200/_cluster/state?pretty"
